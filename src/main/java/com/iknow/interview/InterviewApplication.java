package com.iknow.interview;

import com.iknow.interview.services.ContactService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.ParseException;

@SpringBootApplication
public class InterviewApplication {

	public static void main(String[] args) {
		try
		{
			ContactService.init();
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		SpringApplication.run(InterviewApplication.class, args);
	}

}
