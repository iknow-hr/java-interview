package com.iknow.interview.controllers;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import com.iknow.interview.models.Contact;
import com.iknow.interview.services.ContactService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController("/contact")
public class ContactController
{
    @GetMapping("/")
    public List<Contact> get(){
        return ContactService.getContacts();
    }
    @PostMapping("/")
    public void add(@RequestBody Contact contact){
        ContactService.addContact(contact);
    }
}
