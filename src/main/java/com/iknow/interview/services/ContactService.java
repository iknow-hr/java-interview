package com.iknow.interview.services;

import com.iknow.interview.models.Contact;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContactService
{
    private static List<Contact> contacts=new ArrayList<>();

    public static void init() throws ParseException
    {
        contacts.add(new Contact(1,"John","Doe","jd@mail.com", new SimpleDateFormat("dd/MM/yyyy").parse("11/12/1990") ,"05374585279",2000.48));
        contacts.add(new Contact(1,"Ali","Yunus","ay@mail.com", new SimpleDateFormat("dd/MM/yyyy").parse("05/02/1968") ,"05321234123",4004.44));
        contacts.add(new Contact(1,"Mehmet","Samioğlu","ms@mail.com", new SimpleDateFormat("dd/MM/yyyy").parse("01/07/1957") ,"05552221133",25000.00));
    }

    public static List<Contact> getContacts(){
        return contacts;
    }

    public static void addContact(Contact contact){
        contacts.add(contact);
    }

}
